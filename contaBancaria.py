class LimiteChequeEspecialExcedente(Exception):
    '''
    Exceção deve ser lançada quando é tentado atribuir um limite de 
    especial maior do que o máximo definido na propriedade de classe 
    que controla esse valor 
    '''

class SaldoInsuficiente(Exception):
    '''
    Exceção deve ser lançada quando um saque é tentado numa conta que 
    não possui saldo suficiente (contando inclusive com o valor do cheque especial, 
    caso seja uma ContaCorrente).
    '''

class ContaBancaria(): #Não foi solicitado que a classe 'ContaBancaria' fosse abstrata!
    def __init__(self,numero):
        self.numero=numero
        self.saldo=0
    
    @property
    def numero(self):
        return self.__numero
    
    @numero.setter
    def numero(self,n):
        assert n[:4].isnumeric() and n[5] == '-' and (n[6].isnumeric() or n[6].upper() == 'X'),\
'Formado exigido: 99999-9 ou 99999-X'
        self.__numero=n[:7] #garante que não seja possível passar numeros no formato 12345-100

    @property
    def saldo(self):
        return self.__saldo
    
    @saldo.setter
    def saldo(self,s):
        assert isinstance(s,(int,float)) and s >= 0,\
'O valor precisa ser um número maior ou igual a zero'
        self.__saldo=s
    
    def depositar(self,v):
        assert isinstance(v,(int,float)) and v > 0,\
'O valor precisa ser um número maior do que zero!'
        self.saldo += v

    def sacar(self,v):
        assert isinstance(v,(int,float)) and v > 0, \
'O valor precisa ser um número maior do que zero!'
        if v > self.saldo: raise SaldoInsuficiente('Valor informado maior do que o saldo atual') 
        self.saldo -= v

    def __str__(self):
        info=f'''
Tipo da conta: {self.__class__.__name__}
Numero da conta: {self.numero} 
Saldo: R${self.saldo:.2f}              
'''
        return info

class ContaPoupanca(ContaBancaria):
    def __init__(self,numero,taxa_juros):
        super().__init__(numero)
        self.taxa_juros=taxa_juros

    @property
    def taxa_juros(self):
        return self.__taxa_juros
    
    @taxa_juros.setter
    def taxa_juros(self, t):
        assert isinstance(t,(int,float)) and 0 < t <= 1,\
'A taxa de juros precisa ser um número entre 0 e 1'
        self.__taxa_juros=t

    def ganho_anual(self):
        return f'''
Ganho anual (com taxa de juros de {(self.taxa_juros*100)}% ao ano):
Saldo inicial: R${float(self.saldo):.2f}
Saldo final: R${float(self.saldo+(self.saldo*(self.taxa_juros))):.2f}
Ganho de: R${float((self.saldo*(self.taxa_juros))):.2f}
'''

    def __str__(self):
        return super().__str__() + f'''\
Taxa de juros: {self.taxa_juros*100}%
'''


class ContaCorrente(ContaBancaria):
    max_limite_cheque_especial=1000
    
    def __init__(self,numero,limite):
        super().__init__(numero)
        self.limite_cheque_especial=limite
    
    @property
    def limite_cheque_especial(self):
        return self.__limite_cheque_especial
    
    @limite_cheque_especial.setter
    def limite_cheque_especial(self,l):
        if l > self.__class__.max_limite_cheque_especial: 
            raise LimiteChequeEspecialExcedente('Valor informado é maior do que o permitido!')
        self.__limite_cheque_especial=l
    
    def sacar(self, v):
        '''
        Verifica se o valor passado é menor ou igual ao limite do cheque,
        em seguida chama o método da super classe passando o valor.
        '''
        if v > self.limite_cheque_especial:
            raise SaldoInsuficiente('Valor maior do que o permitido!')
        return super().sacar(v)

    def __str__(self):
        return super().__str__() + f'''\
Limite do cheque: R${self.limite_cheque_especial:.2f}
'''
    
if __name__ == '__main__':
    cb=ContaBancaria('19285-1')
    cc=ContaCorrente('12345-X',100)
    cp=ContaPoupanca('67890-5',0.2)

    #testes com a conta bancaria genérica

    cb.depositar(1000)
    cb.sacar(100)
    print(cb)

    #testes com a conta corrente
    
    cc.depositar(100)
    cc.sacar(10)
    cc.limite_cheque_especial=99.99
    cc.sacar(89.99)
    print(cc)

    #testes com a conta poupança

    cp.depositar(1000)
    cp.sacar(100)
    cp.taxa_juros=0.25
    print(cp.ganho_anual())

    print(cp)
